﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportOrganizationalUnit
{
    public class SQLreader
    {
        private static readonly string connString = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        public List<OrgUnit> GetData()
        {
            List<OrgUnit> rows = new List<OrgUnit>();

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = new SqlCommand("SELECT  [OU_name],[parentou_name],[Group_Manager_DN],[Is_active] FROM [OICT_AD].[dbo].[vw_organizational_units]", connection))
            {


                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        rows.Add(new OrgUnit()
                        {
                            OU_name= reader["OU_name"].ToString(),
                            parentOU_name= reader["parentou_name"].ToString(),
                            Group_Manager_DN= reader["Group_Manager_DN"].ToString(),
                            isActive= reader["Is_active"].ToString()
                        });

                    }
                }




            }


                //vw_organizational_units




                return rows;
        }
    }
}
