﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportOrganizationalUnit
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileLocationWithName = ConfigurationManager.AppSettings["FileLocation"] + ConfigurationManager.AppSettings["FileName"];
            var recorder = new SQLreader();
            List<OrgUnit> data = recorder.GetData();
            
            var csv = new StringBuilder();
            using (StreamWriter writer = new StreamWriter(new FileStream(fileLocationWithName, FileMode.Create, FileAccess.Write)))
            {
               

                foreach (var item in data)
                {
                    var line = string.Format("{0};{1};{2};{3}", item.OU_name, item.parentOU_name, item.Group_Manager_DN, item.isActive.ToString());
                    writer.WriteLine(line);
                   

                }
            }
           
           




        }
    }
}
