﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportOrganizationalUnit
{
    public class OrgUnit
    {

        public string OU_name { get; set; }
        public string parentOU_name { get; set; }
        public string Group_Manager_DN { get; set; }
        public string isActive { get; set; }
    }
}
/*  [OU_name]
      ,[parentou_name]
      ,[Group_Manager_DN]
      ,[Is_active]*/
